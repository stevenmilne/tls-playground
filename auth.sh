#!/bin/bash
set -e

SCRIPT_NAME="auth script"
SCRIPT_VERSION="1.1.1"

KEYSTORE_DIR="$HOME/.keystore"

COMMAND=$1
SUBCOMMAND=$2

if [ ! -d "$KEYSTORE_DIR" ]; then
  echo "Keystore not found, creating a new one..."

  mkdir "$KEYSTORE_DIR"
  mkdir "$KEYSTORE_DIR/authorities"
  mkdir "$KEYSTORE_DIR/certificates"
  mkdir "$KEYSTORE_DIR/keypairs"
  mkdir "$KEYSTORE_DIR/keys"

  chmod -R 700 "$KEYSTORE_DIR"
fi

if [ -z "$COMMAND" ] || [ "$COMMAND" = "help" ]; then
  echo "$SCRIPT_NAME (version $SCRIPT_VERSION)"
  echo " A collection of scripts to manage cryptographic keys and certificates."
  echo
  echo " auth install symlink"
  echo " auth uninstall symlink"
  echo " auth keystore ..."
  echo "   import <authority|certificate|keypair|key> <name>"
  echo "   export <authority|certificate|keypair|key> <name>"
  echo "   delete <authority|certificate|keypair|key> <name>"
  echo "   list [authority|certificate|keypair|key]"
  echo " auth create ..."
  echo "   authority <name> [parent]"
  echo "   certificate <name> [authority] [type]"
  echo "   keypair <name> [size]"
  echo "   key <name> [size]"
  echo "   digest <file> [key]"
  echo " auth certificate ..."
  echo "   sign <name> <authority> [type]"
  echo "   validate <name> <auth>"
  echo "   revoke <name> <authority>"
  echo "   encrypt <cert> <file> [text|binary]"
  echo "   decrypt <key> <file> [text|binary]"
  echo "   extract <name>"
  echo "   show <name>"
  echo " auth keypair ..."
  echo "   encrypt <key> <file>"
  echo "   decrypt <key> <file>"
  echo " auth key ..."
  echo "   encrypt <key> <file>"
  echo "   decrypt <key> <file>"
  echo " auth digest ..."
  echo "   validate <digest> [cert]"
  echo
  echo "Examples:"
  echo " auth create authority root"
  echo " auth create authority intermediate root"
  echo " auth create certificate example.com intermediate"
  echo " auth certificate encrypt example.com message.txt"
  echo " auth certificate validate example.com intermediate"
  echo ""
elif [ "$COMMAND" = "install" ]; then
  if [ -z "$SUBCOMMAND" ] || [ "$SUBCOMMAND" = "help" ]; then
    echo " auth install symlink"
  elif [ "$SUBCOMMAND" = "symlink" ]; then
    if [ ! -f "auth.sh" ] || [ ! -f "openssl.conf" ]; then
      echo "Cannot find script, run this command in the repository"
      exit 1
    fi

    echo "Installing symlink..."
    cp auth.sh /usr/local/bin/auth
    cp openssl.conf /usr/local/var/openssl.conf
  else
    echo "Command 'auth $COMMAND $SUBCOMMAND' not recognised, try 'auth $COMMAND help'."
  fi
elif [ "$COMMAND" = "uninstall" ]; then
  if [ -z "$SUBCOMMAND" ] || [ "$SUBCOMMAND" = "help" ]; then
    echo " auth uninstall symlink"
  elif [ "$SUBCOMMAND" = "symlink" ]; then
    if [ -f /usr/local/bin/auth ]; then
      echo "Uninstalling symlink..."
      rm -f /usr/local/bin/auth
    fi
    if [ -f /usr/local/var/openssl.conf ]; then
      echo "Uninstalling data..."
      rm -f /usr/local/var/openssl.conf
    fi
  else
    echo "Command 'auth $COMMAND $SUBCOMMAND' not recognised, try 'auth $COMMAND help'."
  fi
elif [ "$COMMAND" = "keystore" ]; then
  if [ -z "$SUBCOMMAND" ] || [ "$SUBCOMMAND" = "help" ]; then
    echo "auth keystore import <authority|certificate|keypair|key> <name>"
    echo "auth keystore export <authority|certificate|keypair|key> <name>"
    echo "auth keystore delete <authority|certificate|keypair|key> <name>"
    echo "auth keystore list [authority|certificate|keypair|key]"
  elif [ "$SUBCOMMAND" = "import" ]; then
    TYPE=$3
    NAME=$4

    if [ -z "$TYPE" ] || [ -z "$NAME" ]; then
      echo "Usage: auth keystore import <authority|certificate|keypair|key> <name>"
      exit 1
    fi

    if [ "$TYPE" = "authority" ]; then
      if [ ! -d "$NAME" ]; then
        echo "Error: Certificate authority directory '$NAME' could not be found."
        exit 1
      fi

      echo "Importing certificate authority $NAME..."
      cp -R "$NAME" "$KEYSTORE_DIR/authorities/$NAME/"
      chmod -R 700 "$KEYSTORE_DIR/authorities/$NAME"
    elif [ "$TYPE" = "certificate" ]; then
      if [ ! -f "$NAME.crt" ]; then
        echo "Error: Certificate '$NAME.crt' could not be found."
        exit 1
      fi

      echo "Importing certificate $NAME..."
      cp "$NAME.crt" "$KEYSTORE_DIR/certificates/$NAME.crt"
      chmod 600 "$KEYSTORE_DIR/certificates/$NAME.crt"
    elif [ "$TYPE" = "keypair" ]; then
      if [ ! -f "$NAME.key" ] && [ ! -f "$NAME.pub" ]; then
        echo "Error: Keypair '$NAME.key' and '$NAME.pub' could not be found."
        exit 1
      fi

      if [ -f "$NAME.key" ]; then
        echo "Importing private key $NAME..."
        cp "$NAME.key" "$KEYSTORE_DIR/keypairs/$NAME.key"
        chmod 600 "$KEYSTORE_DIR/keypairs/$NAME.key"
      fi
      
      if [ -f "$NAME.pub" ]; then
        echo "Importing public key $NAME..."
        cp "$NAME.pub" "$KEYSTORE_DIR/keypairs/$NAME.pub"
        chmod 600 "$KEYSTORE_DIR/keypairs/$NAME.pub"
      fi
    elif [ "$TYPE" = "key" ]; then
      if [ ! -f "$NAME.bin" ]; then
        echo "Error: Key '$NAME.bin' could not be found."
        exit 1
      fi

      echo "Importing key $NAME..."
      cp "$NAME.bin" "$KEYSTORE_DIR/keys/$NAME.bin"
      chmod 600 "$KEYSTORE_DIR/keys/$NAME.bin"
    else
      echo "Error: Invalid type '$TYPE', must be one of authority, certificate, keypair, key."
      exit 1
    fi
  elif [ "$SUBCOMMAND" = "export" ]; then
    TYPE=$3
    NAME=$4

    if [ -z "$TYPE" ] || [ -z "$NAME" ]; then
      echo "Usage: auth keystore export <authority|certificate|keypair|key> <name>"
      exit 1
    fi

    if [ "$TYPE" = "authority" ]; then
      if [ ! -d "$KEYSTORE_DIR/authorities/$NAME" ]; then
        echo "Error: Certificate authority '$NAME' is not in keystore."
        exit 1
      fi

      echo "Exporting certificate authority $NAME..."
      cp -R "$KEYSTORE_DIR/authorities/$NAME" "$NAME"
    elif [ "$TYPE" = "certificate" ]; then
      if [ ! -f "$KEYSTORE_DIR/certificates/$NAME.crt" ]; then
        echo "Error: Certificate '$NAME' is not in keystore."
        exit 1
      fi

      echo "Exporting certificate $NAME..."
      cp "$KEYSTORE_DIR/certificates/$NAME.crt" "$NAME.crt"
    elif [ "$TYPE" = "keypair" ]; then
      if [ ! -f "$KEYSTORE_DIR/keypairs/$NAME.key" ] && [ ! -f "$KEYSTORE_DIR/keypairs/$NAME.pub" ]; then
        echo "Error: Keypair '$NAME' is not in keystore."
        exit 1
      fi

      if [ -f "$KEYSTORE_DIR/keypairs/$NAME.key" ]; then
        echo "Exporting private key $NAME..."
        cp "$KEYSTORE_DIR/keypairs/$NAME.key" "$NAME.key"
      fi
      
      if [ -f "$KEYSTORE_DIR/keypairs/$NAME.pub" ]; then
        echo "Exporting public key $NAME..."
        cp "$KEYSTORE_DIR/keypairs/$NAME.pub" "$NAME.pub"
      fi
    elif [ "$TYPE" = "key" ]; then
      if [ ! -f "$KEYSTORE_DIR/keys/$NAME.bin" ]; then
        echo "Error: Key '$NAME' is not in keystore."
        exit 1
      fi

      echo "Exporting key $NAME..."
      cp "$KEYSTORE_DIR/keys/$NAME.bin" "$NAME.bin"
    else
      echo "Error: Invalid type '$TYPE', must be one of authority, certificate, keypair, key."
      exit 1
    fi
  elif [ "$SUBCOMMAND" = "delete" ]; then
    TYPE=$3
    NAME=$4

    if [ -z "$TYPE" ] || [ -z "$NAME" ]; then
      echo "Usage: auth keystore delete <authority|certificate|keypair|key> <name>"
      exit 1
    fi

    if [ "$TYPE" = "authority" ]; then
      if [ ! -d "$KEYSTORE_DIR/authorities/$NAME" ]; then
        echo "Error: Certificate authority '$NAME' is not in keystore."
        exit 1
      fi

      echo "Deleting certificate authority $NAME..."
      rm -fr "$KEYSTORE_DIR/authorities/$NAME"
    elif [ "$TYPE" = "certificate" ]; then
      if [ ! -f "$KEYSTORE_DIR/certificates/$NAME.crt" ]; then
        echo "Error: Certificate '$NAME' is not in keystore."
        exit 1
      fi

      echo "Deleting certificate $NAME..."
      rm "$KEYSTORE_DIR/certificates/$NAME.crt"
    elif [ "$TYPE" = "keypair" ]; then
      if [ ! -f "$KEYSTORE_DIR/keypairs/$NAME.key" ] && [ ! -f "$KEYSTORE_DIR/keypairs/$NAME.pub" ]; then
        echo "Error: Keypair '$NAME' is not in keystore."
        exit 1
      fi

      if [ -f "$KEYSTORE_DIR/keypairs/$NAME.key" ]; then
        echo "Deleting private key $NAME..."
        rm "$KEYSTORE_DIR/keypairs/$NAME.key"
      fi
      
      if [ -f "$KEYSTORE_DIR/keypairs/$NAME.pub" ]; then
        echo "Deleting public key $NAME..."
        rm "$KEYSTORE_DIR/keypairs/$NAME.pub"
      fi
    elif [ "$TYPE" = "key" ]; then
      if [ ! -f "$KEYSTORE_DIR/keys/$NAME.bin" ]; then
        echo "Error: Key '$NAME' is not in keystore."
        exit 1
      fi

      echo "Deleting key $NAME..."
      rm "$KEYSTORE_DIR/keys/$NAME.bin"
    else
      echo "Error: Invalid type '$TYPE', must be one of authority, certificate, keypair, key."
      exit 1
    fi
  elif [ "$SUBCOMMAND" = "list" ]; then
    TYPE=$3

    if [ "$TYPE" = "authority" ]; then
      ls -1 "$KEYSTORE_DIR/authorities"
    elif [ "$TYPE" = "certificate" ]; then
      ls -1 "$KEYSTORE_DIR/certificates" | sed -e 's/\.crt$//'
    elif [ "$TYPE" = "keypair" ]; then
      find "$KEYSTORE_DIR/keypairs" -name "*.key" | sed 's/\.key$//' | sed 's/.*\///'
    elif [ "$TYPE" = "key" ]; then
      ls -1 "$KEYSTORE_DIR/keys" | sed -e 's/\.bin$//'
    else
      echo -e "\033[0;1;4mCertificate Authorities\033[0m"
      ls -1 "$KEYSTORE_DIR/authorities"
      echo
      echo -e "\033[0;1;4mCertificates\033[0m"
      ls -1 "$KEYSTORE_DIR/certificates" | sed -e 's/\.crt$//'
      echo
      echo -e "\033[0;1;4mPrivate Keys\033[0m"
      find "$KEYSTORE_DIR/keypairs" -name "*.key" | sed 's/\.key$//' | sed 's/.*\///'
      echo
      echo -e "\033[0;1;4mPublic Keys\033[0m"
      find "$KEYSTORE_DIR/keypairs" -name "*.pub" | sed 's/\.pub$//' | sed 's/.*\///'
      echo
      echo -e "\033[0;1;4mKeys\033[0m"
      ls -1 "$KEYSTORE_DIR/keys" | sed -e 's/\.bin$//'
    fi
  else
    echo "Command 'auth $COMMAND $SUBCOMMAND' not recognised, try 'auth $COMMAND help'."
  fi
elif [ "$COMMAND" = "create" ]; then
  if [ -z "$SUBCOMMAND" ] || [ "$SUBCOMMAND" = "help" ]; then
    echo " auth create authority <name> [parent]"
    echo " auth create certificate <name> [authority] [type]"
    echo " auth create keypair <name> [size]"
    echo " auth create key <name> [size]"
    echo " auth create digest <file> [key]"
  elif [ "$SUBCOMMAND" = "authority" ]; then
    NAME=$3
    PARENT=$4

    if [ -z "$NAME" ]; then
      echo "Usage: auth create authority <name> [parent]"
      exit 1
    fi

    if [ ! -z "$PARENT" ] && [ !  -d "$PARENT" ]; then
      echo "Could not find certificate authority '$PARENT'"
      exit 1
    fi

    if [ ! -d "$NAME" ]; then
      mkdir "$NAME"
      mkdir "$NAME/certs"
      mkdir "$NAME/crl"
      mkdir "$NAME/csr"
      mkdir "$NAME/newcerts"
      mkdir "$NAME/private"

      touch "$NAME/index.txt"
      echo 1000 > "$NAME/crlnumber"
      echo 1000 > "$NAME/serial"

      if [ -z "$PARENT" ]; then
        POLICY="policy_strict"
      else
        POLICY="policy_loose"
      fi

      if [ -f /usr/local/var/openssl.conf ]; then
        cp /usr/local/var/openssl.conf "$NAME/openssl.conf"
      elif [ -f openssl.conf ]; then
        cp openssl.conf "$NAME/openssl.conf"
      else
        echo "openssl.conf not found! Either run this command from the repository"
        echo "or install the symlink."
        exit 1
      fi
      sed -i "" 's/CERT_NAME/'"$NAME"'/g' "$NAME/openssl.conf"
      sed -i "" 's/POLICY/'"$POLICY"'/g' "$NAME/openssl.conf"
    fi

    if [ ! -f "$NAME/private/$NAME.key" ]; then
      echo "Generating private key..."
      openssl genrsa -aes256 -out "$NAME/private/$NAME.key" 4096
      chmod 400 "$NAME/private/$NAME.key"
    fi

    cd "$NAME"

    echo -n "Edit the configuration? (y/n)[n]: "
    read EDIT

    if [ ! -z "$EDIT" ] && [ "$EDIT" = "y" ]; then
      nano openssl.conf
    fi

    if [ -z "$PARENT" ]; then
      if [ ! -f "certs/$NAME.crt" ]; then
        echo ""
        echo "Generating certificate..."
        openssl req -new -x509 -days 7300 -sha256 \
          -config openssl.conf \
          -key "private/$NAME.key" \
          -extensions v3_ca \
          -out "certs/$NAME.crt"

        chmod 444 "certs/$NAME.crt"
      fi
    else
      if [ ! -f "csr/$NAME.csr" ]; then
        echo ""
        echo "Generating certificate signing request..."
        openssl req -config openssl.conf -new -sha256 \
          -key "private/$NAME.key" \
          -out "csr/$NAME.csr"
      fi
      
      cd "../$PARENT"
      if [ ! -f "../$NAME/certs/$NAME.crt" ]; then
        echo ""
        echo "Signing certificate..."
        openssl ca -config openssl.conf -extensions v3_intermediate_ca \
          -days 3650 -notext -md sha256 \
          -in "../$NAME/csr/$NAME.csr" \
          -out "../$NAME/certs/$NAME-no-chain.crt"
        chmod 444 "../$NAME/certs/$NAME-no-chain.crt"
        cat "../$NAME/certs/$NAME-no-chain.crt" "certs/$PARENT.crt" > "../$NAME/certs/$NAME.crt"
        chmod 444 "../$NAME/certs/$NAME.crt"
      fi
      cd "../$NAME"
    fi

    if [ ! -f "crl/$NAME.crl" ]; then
      echo ""
      echo "Generating CRL..."
      openssl ca -config openssl.conf -gencrl -out "crl/$NAME.crl"
    fi

    if [ ! -f "csr/ocsp.$NAME.csr" ]; then
      echo ""
      echo "Generating OCSP certificate..."
      openssl genrsa -aes256 -out "private/ocsp.$NAME.key" 4096
      openssl req -config openssl.conf -new -sha256 \
        -key "private/ocsp.$NAME.key" \
        -out "csr/ocsp.$NAME.csr"
    fi

    if [ ! -f "certs/ocsp.$NAME.crt" ]; then
      openssl ca -config openssl.conf \
        -extensions ocsp -days 365 -notext -md sha256 \
        -in "csr/ocsp.$NAME.csr" \
        -out "certs/ocsp.$NAME.crt"
    fi

    cd ..
  elif [ "$SUBCOMMAND" = "certificate" ]; then
    NAME=$3
    CA=$4
    EXT=$5

    if [ -z "$NAME" ]; then
      echo "Usage: auth create certificate <name> [authority] [type]"
      exit 1
    fi

    if [ -z "$EXT" ]; then
      EXT="server_cert"
    fi

    if [ ! -f "$NAME.key" ]; then
      echo "Creating private key..."
      openssl genrsa -out "$NAME.key" 2048 &> /dev/null
    fi

    if [ ! -f "$NAME.csr" ]; then
      echo "Creating signing request..."
      echo ""
      openssl req -new -key "$NAME.key" -out "$NAME.csr"
      echo ""
    fi

    if [ ! -z "$CA" ] && [ -d "$CA" ]; then
      echo "Signing certificate..."
      cd "$CA"
      openssl ca -config openssl.conf -extensions $EXT \
        -days 365 -notext -md sha256 \
        -in "../$NAME.csr" \
        -out "../$NAME.crt"
      chmod 444 "../$NAME.crt"
      cd ..
    else
      echo "Certificate signing request created ($NAME.csr), please sign this with a certificate authority."
    fi
  elif [ "$SUBCOMMAND" = "keypair" ]; then
    NAME=$3
    SIZE=$4

    if [ -z "$NAME" ]; then
      echo "Usage auth create keypair <name> [size]"
      exit 1
    fi

    if [ -z "$SIZE" ]; then
      SIZE=2048
    fi

    if [ ! -f "$NAME.key" ]; then
      echo "Generating private key..."
      openssl genrsa -aes256 -out "$NAME.key" "$SIZE"
    fi

    if [ ! -f "$NAME.pub" ]; then
      echo "Generating public key..."
      openssl rsa -in "$NAME.key" -pubout -out "$NAME.pub"
    fi
  elif [ "$SUBCOMMAND" = "key" ]; then
    NAME=$3
    SIZE=$4

    if [ -z "$NAME" ]; then
      echo "Usage auth create key <name> [size]"
      exit 1
    fi

    if [ -z "$SIZE" ]; then
      SIZE=64
    fi

    if [ ! -f "$NAME.bin" ]; then
      echo "Generating symmetric key..."
      openssl rand -hex "$SIZE" -out "$NAME.bin"
    fi
  elif [ "$SUBCOMMAND" = "digest" ]; then
    FILE=$3
    KEY=$4

    if [ -z "$FILE" ]; then
      echo "Usage: auth create digest <file> [key]"
      exit 1
    fi

    if [ ! -f "$FILE" ]; then
      echo "Could not find file '$FILE'"
      exit 1
    fi

    if [ -z "$KEY" ]; then
      echo "Generating digest..."
      openssl dgst -sha256 -out "$FILE.sha256" "$FILE"
    else
      if [ ! -f "$KEY.key" ]; then
        echo "Could not find key '$KEY.key'"
        exit 1
      fi

      echo "Generating signed digest..."
      openssl dgst -sha256 -sign "$KEY.key" -out "$FILE.sha256" "$FILE"
    fi
  else
    echo "Command 'auth $COMMAND $SUBCOMMAND' not recognised, try 'auth $COMMAND help'."
  fi
elif [ "$COMMAND" = "certificate" ]; then
  if [ -z "$SUBCOMMAND" ] || [ "$SUBCOMMAND" = "help" ]; then
    echo "auth certificate sign <name> <authority> [type]"
    echo "auth certificate validate <name> <auth>"
    echo "auth certificate revoke <name> <authority>"
    echo "auth certificate encrypt <cert> <file> [text|binary]"
    echo "auth certificate decrypt <key> <file> [text|binary]"
    echo "auth certificate extract <name>"
    echo "auth certificate show <name>"
  elif [ "$SUBCOMMAND" = "sign" ]; then
    NAME=$3
    CA=$4
    EXT=$5

    if [ -z "$NAME" ]; then
      echo "Usage: auth certificate sign <name> <authority> [v3_ca|v3_intermediate_ca|usr_cert|server_cert]"
      exit 1
    fi

    if [ ! -f "$NAME" ]; then
      echo "Could not find signing request '$NAME.csr'"
      exit 1
    fi

    if [ -z "$CA" ]; then
      echo "Usage: auth certificate sign <name> <authority> [v3_ca|v3_intermediate_ca|usr_cert|server_cert]"
      exit 1
    fi

    if [ ! -d "$CA" ]; then
      echo "Could not find authority '$CA'"
      exit 1
    fi

    if [ -z "$EXT" ]; then
      EXT="server_cert"
    fi

    echo "Signing certificate..."
    cd "$CA"
    openssl ca -config openssl.conf -extensions $EXT \
      -days 365 -notext -md sha256 \
      -in "../$NAME.csr" \
      -out "../$NAME.crt"
    chmod 444 "../$NAME.crt"
    cd ..
  elif [ "$SUBCOMMAND" = "validate" ]; then
    NAME=$3
    CA=$4

    if [ -z "$NAME" ] || [ -z "$CA" ]; then
      echo "Usage: auth certificate validate <name> <authority>"
      exit 1
    fi

    if [ ! -f "$NAME.crt" ]; then
      echo "Could not find certificate '$NAME.crt'"
    fi

    if [ ! -d "$CA" ]; then
      echo "Could not find certificate authority '$CA'"
    fi

    echo "Starting OCSP server..."
    openssl ocsp -port 127.0.0.1:2560 -text -sha256 \
      -index "$CA/index.txt" \
      -CA "$CA/certs/$CA.crt" \
      -rkey "$CA/private/ocsp.$CA.key" \
      -rsigner "$CA/certs/ocsp.$CA.crt" \
      -nrequest 1 &> /dev/null &

    echo "Checking status..."
    openssl ocsp -CAfile "$CA/certs/$CA.crt" \
      -url http://127.0.0.1:2560 \
      -issuer "$CA/certs/$CA.crt" \
      -cert "$NAME.crt"
  elif [ "$SUBCOMMAND" = "revoke" ]; then
    NAME=$3
    CA=$4

    if [ -z "$NAME" ]; then
      echo "Usage: auth certificate revoke <name> <authority>"
      exit 1
    fi

    if [ ! -f "$NAME.crt" ]; then
      echo "Could not find signing request '$NAME.crt'"
      exit 1
    fi

    if [ -z "$CA" ]; then
      echo "Usage: auth certificate revoke <name> <authority>"
      exit 1
    fi

    if [ ! -d "$CA" ]; then
      echo "Could not find authority '$CA'"
      exit 1
    fi

    cd "$CA"
    echo "Revoking certificate..."
    openssl ca -config openssl.conf \
      -revoke "../$NAME.crt"

    echo "Updating CRL..."
    openssl ca -config openssl.conf -gencrl -out "crl/$CA.crl"
    cd ..
  elif [ "$SUBCOMMAND" = "encrypt" ]; then
    CERT=$3
    FILE=$4
    TYPE=$5

    if [ -z "$CERT" ] || [ -z "$FILE" ]; then
      echo "Usage: auth certificate encrypt <cert> <file> [text|binary]"
      exit 1
    fi

    if [ -d "$CERT" ]; then
      CERT="$CERT/certs/$CERT"
    fi

    if [ ! -f "$CERT.crt" ]; then
      echo "Could not find certificate '$CERT.crt'"
      exit 1
    fi

    if [ ! -f "$FILE" ]; then
      echo "Could not find file '$FILE'"
      exit 1
    fi

    if [ -z "$TYPE" ]; then
      OPTIONS=""
    elif [ "$TYPE" = "text" ]; then
      OPTIONS=""
    elif [ "$TYPE" = "binary"]; then
      OPTIONS="-binary"
    else
      echo "Usage: auth certificate encrypt <key> <file> [text|binary]"
      exit 1
    fi

    if [ ! -f "$FILE.enc" ]; then
      echo "Encrypting file..."
      openssl smime -encrypt $OPTIONS -aes-256-cbc -in "$FILE" -out "$FILE.enc" "$CERT.crt"
    fi
  elif [ "$SUBCOMMAND" = "decrypt" ]; then
    KEY=$3
    FILE=$4
    TYPE=$5

    if [ -z "$KEY" ] || [ -z "$FILE" ]; then
      echo "Usage: auth certificate decrypt <key> <file> [text|binary]"
      exit 1
    fi

    if [ -z "$TYPE" ]; then
      OPTIONS=""
    elif [ "$TYPE" = "text" ]; then
      OPTIONS=""
    elif [ "$TYPE" = "binary"]; then
      OPTIONS="-binary"
    else
      echo "Usage: auth certificate decrypt <key> <file> [text|binary]"
      exit 1
    fi

    if [ -d "$KEY" ]; then
      KEY="$KEY/private/$KEY"
    fi

    if [ ! -f "$KEY.key" ]; then
      echo "Could not find key '$KEY.key'"
      exit 1
    fi

    if [ ! -f "$FILE.enc" ]; then
      echo "Could not find file '$FILE.enc'"
      exit 1
    fi

    if [ ! -f "$FILE" ]; then
      echo "Decrypting file..."
      openssl smime -decrypt $OPTIONS -in "$FILE.enc" -out "$FILE" -inkey "$KEY.key"
    fi
  elif [ "$SUBCOMMAND" = "extract" ]; then
    NAME=$3

    if [ -z "$NAME" ]; then
      echo "Usage: auth certificate extract <name>"
      exit 1
    fi

    if [ ! -f "$NAME.crt" ]; then
      echo "Could not find the certificate '$NAME.crt'"
      exit 1
    fi

    if [ ! -f "$NAME.pub" ]; then
      openssl x509 -pubkey -noout -in "$NAME.crt" > "$NAME.pub"
    fi
  elif [ "$SUBCOMMAND" = "show" ]; then
    NAME=$3

    if [ -z "$NAME" ]; then
      echo "Usage: auth certificate show <name>"
      exit 1
    fi

    if [ ! -f "$NAME.crt" ]; then
      if [ ! -f "$NAME.csr" ]; then
        echo "Could not find the certificate '$NAME.crt'"
        exit 1
      fi

      openssl req -in "$NAME.csr" -text -noout
    else
      openssl x509 -in "$NAME.crt" -text -noout
    fi
  else
    echo "Command 'auth $COMMAND $SUBCOMMAND' not recognised, try 'auth $COMMAND help'."
  fi
elif [ "$COMMAND" = "keypair" ]; then
  if [ -z "$SUBCOMMAND" ] || [ "$SUBCOMMAND" = "help" ]; then
    echo " auth keypair encrypt <key> <file>"
    echo " auth keypair decrypt <key> <file>"
  elif [ "$SUBCOMMAND" = "encrypt" ]; then
    KEY=$3
    FILE=$4

    if [ -z "$KEY" ] || [ -z "$FILE" ]; then
      echo "Usage: auth keypair encrypt <key> <file>"
      exit 1
    fi

    if [ ! -f "$KEY.pub" ]; then
      echo "Could not find key '$KEY.pub'"
      exit 1
    fi

    if [ ! -f "$FILE" ]; then
      echo "Could not find file '$FILE'"
      exit 1
    fi

    if [ ! -f "$FILE.enc" ]; then
      echo "Encrypting file..."
      openssl rsautl -encrypt -pubin -inkey "$KEY.pub" -in "$FILE" -out "$FILE.enc"
    fi
  elif [ "$SUBCOMMAND" = "decrypt" ]; then
    KEY=$3
    FILE=$4

    if [ -z "$KEY" ] || [ -z "$FILE" ]; then
      echo "Usage: auth keypair decrypt <key> <file>"
      exit 1
    fi

    if [ ! -f "$KEY.pub" ]; then
      echo "Could not find key '$KEY.pub'"
      exit 1
    fi

    if [ ! -f "$FILE.enc" ]; then
      echo "Could not find file '$FILE.enc'"
      exit 1
    fi

    if [ ! -f "$FILE" ]; then
      echo "Decrypting file..."
      openssl rsautl -decrypt -inkey "$KEY.key" -in "$FILE.enc" -out "$FILE"
    fi
  else
    echo "Command 'auth $COMMAND $SUBCOMMAND' not recognised, try 'auth $COMMAND help'."
  fi
elif [ "$COMMAND" = "key" ]; then
  if [ -z "$SUBCOMMAND" ] || [ "$SUBCOMMAND" = "help" ]; then
    echo " auth key encrypt <key> <file>"
    echo " auth key decrypt <key> <file>"
  elif [ "$SUBCOMMAND" = "encrypt" ]; then
    KEY=$3
    FILE=$4

    if [ -z "$KEY" ] || [ -z "$FILE" ]; then
      echo "Usage: auth key encrypt <key> <file>"
      exit 1
    fi

    if [ ! -f "$KEY.bin" ]; then
      echo "Could not find key '$KEY.bin'"
      exit 1
    fi

    if [ ! -f "$FILE" ]; then
      echo "Could not find file '$FILE'"
      exit 1
    fi

    if [ ! -f "$FILE.enc" ]; then
      echo "Encrypting file..."
      openssl enc -aes-256-cbc -in "$FILE" -out "$FILE.enc" -pass "file:./$KEY.bin"
    fi
  elif [ "$SUBCOMMAND" = "decrypt" ]; then
    KEY=$3
    FILE=$4

    if [ -z "$KEY" ] || [ -z "$FILE" ]; then
      echo "Usage: auth key encrypt <key> <file>"
      exit 1
    fi

    if [ ! -f "$KEY.bin" ]; then
      echo "Could not find key '$KEY.bin'"
      exit 1
    fi

    if [ ! -f "$FILE.enc" ]; then
      echo "Could not find file '$FILE.enc'"
      exit 1
    fi

    if [ ! -f "$FILE" ]; then
      echo "Decrypting file..."
      openssl enc -d -aes-256-cbc -in "$FILE.enc" -out "$FILE" -pass "file:./$KEY.bin"
    fi
  else
    echo "Command 'auth $COMMAND $SUBCOMMAND' not recognised, try 'auth $COMMAND help'."
  fi
elif [ "$COMMAND" = "digest" ]; then
  if [ -z "$SUBCOMMAND" ] || [ "$SUBCOMMAND" = "help" ]; then
    echo " auth digest validate <digest> [cert]"
  elif [ "$SUBCOMMAND" = "validate" ]; then
    FILE=$3
    CERT=$4

    if [ -z "$FILE" ]; then
      echo "Usage: auth digest validate <digest> [cert]"
      exit 1
    fi

    if [ ! -f "$FILE.sha256" ]; then
      echo "Could not find digest '$FILE.sha256'"
      exit 1
    fi

    if [ -z "$CERT" ]; then
      echo "Validating digest..."
      NOW=$(openssl dgst -sha256 -signature "$FILE.sha256" "$FILE")
      BEFORE=$(cat "$FILE.sha256")
      if [ "$BEFORE" = "$NOW" ]; then
        echo "Verified OK"
      else
        echo "Verification Failure"
        exit 1
      fi
    else
      if [ ! -f "$CERT.crt" ]; then
        echo "Could not find certificate '$CERT.crt'"
        exit 1
      fi

      echo "Validating signed request..."
      openssl dgst -sha256 -verify <(openssl x509 -in "$CERT.crt" -pubkey -noout) -signature "$FILE.sha256" "$FILE"
    fi
  else
    echo "Command 'auth $COMMAND $SUBCOMMAND' not recognised, try 'auth $COMMAND help'."
  fi
else
  echo "Command 'auth $COMMAND' not recognised, try 'auth help'."
fi