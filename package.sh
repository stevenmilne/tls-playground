#!/bin/bash
set -e

PROGRAM="auth.sh"
DIGEST="$PROGRAM.sha256"
SIGNING_KEY="steven.milne"

if [ -f $DIGEST ]; then
  rm $DIGEST
fi

if [ -f auth.tar.gz ]; then
  rm auth.tar.gz
fi

./$PROGRAM create digest $PROGRAM $SIGNING_KEY

echo "Packaging..."
tar -cf auth.tar $PROGRAM $DIGEST steven.milne.crt openssl.conf README.md
gzip auth.tar
