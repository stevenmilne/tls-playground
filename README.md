# TLS Playground

This project contains a collection of files used to play with [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) on the web. TLS, or Trasport Layer Security, is an application layer on the internet which can be used to securely send and recieve data over the internet. TLS encrypts application data in transit using cryptographic keys. These keys come in the form of an [X.509](https://en.wikipedia.org/wiki/X.509) certificate used by the server.

## Script Usage
This repository contains a helper script (`auth.sh`) that can be installed to make common usage of `openssl` easier. You can install the script globally using the command `./auth.sh install symlink`. Typing the command with no arguments will show a list of subcommands that it accepts.

If you downloaded the script as an `auth.tar.gz` file then you can extract it with the commands;

```sh
gunzip auth.tar.gz
tar -xf auth.tar
```

Once extracted you will have the `auth.sh` file along with an `auth.sh.sha256` and  `steven.milne.crt`. You can use the extra files to verify the authenticity and that the file was created by me. To verify the script, run the following command if you have a pre-existing installation;

> `auth digest validate auth.sh steven.milne`

If you do not already have a copy of the script installed then run this command instead;

```sh
openssl dgst -sha256 \
  -verify <(openssl x509 -in "steven.milne.crt" -pubkey -noout) \
  -signature "auth.sh.sha256" \
  "auth.sh"
```

## Symmetric Encryption
Symmetric encryption uses a single key for both encryption and decryption of data. The key represents a shared secret between two or more parties, each of which must have an identical copy of it. The key is simply a random number. The key can be used to encrypt data using [OpenSSL](https://www.openssl.org/). To create a symmetric key type the following command;

> `openssl rand -hex 64 -out key.bin`

The key can now be used to encrypt a file;

> `openssl enc -aes-256-cbc -in message.txt -out message.txt.enc -pass file:./key.bin`

The file is decrypted with the exact same key;

> `openssl enc -d -aes-256-cbc -in message.txt.enc -out message.txt -pass file:./key.bin`

The main disadvantage of symmetric encryption is that the key must be shared somehow which puts it at a higher risk of being compromised.

## Asymmetric Encryption
Asymmetric encryption makes use of key pairs. These are pairs of encryption keys which can encrypt and decrypt each others content. Keys are usually arranged as a public and a private key. The private key is kept secret and the public key is given to anybody that wants to send the key holder an encrypted message. Only the private key can decrypt messages encrypted with the public key.

Key pairs can be created using the following commands with [OpenSSL](https://www.openssl.org/);

```
openssl genrsa -aes256 -out private.key 8912
openssl rsa -in private.key -pubout -out key.pub
```

These commands create a private key using the RSA algorithm and then extract the public key from it. Now that we have a key pair we can encrypt a message using the public key;

>  `openssl rsautl -encrypt -pubin -inkey key.pub -in message.txt -out message.txt.enc`

Now that the file is encrypted with the public key, nobody except the holder of the private key can decrypt it.  To decrypt the file you can use this command;

> `openssl rsautl -decrypt -inkey private.key -in message.txt.enc -out message.txt`

There is one caveat with asymetric encryption however; it cannot be used to encrypt large files. It can only be used to encrypt files as large as the number of bits used in the encryption algorithm. For the example above, we used a 8912 bit RSA key, which limits our filesize to 8901 bits long. A good compromise is to use both symmetric and asymetric encryption. To encrypt a larger file, first encrypt it with a symmetric key then encrypt that symmetric key with an asymetric one and send both the encrypted key and encrypted payload.

Below is an example shell session in which a large file is encrypted;

```
# Create a symmetric key and encrypt the file
openssl rand -hex 64 -out key.bin
openssl enc -aes-256-cbc -in message.txt -out message.txt.enc -pass file:./key.bin

# Create an asymmetric key and encrypt the symmetric one
openssl genrsa -aes256 -out private.key 8912
openssl rsa -in private.key -pubout -out key.pub
openssl rsautl -encrypt -pubin -inkey key.pub -in key.bin -out key.enc
```

This session leaves us with `message.txt.enc` and `key.enc` files, these are the encrypted message and key. In the example the private and public keys are generated, in reality however you would have received the public key from the destination of the message. The `private.key` file is required to decrypt the symmetric key. Below is an example session which decrypts the message;

```
openssl rsautl -decrypt -inkey private.key -in key.enc -out key.bin
openssl enc -d -aes-256-cbc -in message.txt.enc -out message.txt -pass file:./key.bin
```

These commands decrypt the symmetric key with the private key and then use it to decrypt the message. Using both types of encryption together allow us to send data securely over the internet. There is one thing however that asymmetric keys on their own lack, trust. An attacker may be able to intercept the initial communications you have with somebody and inject their own key instead. If this were to happen, you'd be unaware and encrypt all of your traffic for the attacker instead.

## Certificates

A certificate is a document which contains a public key and information about the organisation who owns it. Unlike a key on it's own this can help prove the identity of the party. To ensure the certificate belongs to who it says it does it is verified and cryptographically signed by a trusted third party. This third party is called a certificate authority.

To create a certificate, you must first create a private key using this command;

> `openssl genrsa -out my.key 2048`

The file generated contains your private key, keep this safe and away from prying eyes. Before you can create a certificate, you must first create a signing request. You can create a certificate signing request with this command;

> `openssl req -new -key my.key -out cert.csr`

This command will ask for some more information about the subject that the certificate is for. The provided information is bundled with your public key in a signing request. The signing request would normally be sent to a certificate authority to be signed at which point it becomes a real certificate.

### Certificate Authority

A certificate authority functions as a trusted third party who verifies that certificates are correct and have not been tampered with. To create a certificate you must send a certificate signing request to a certificate authority. The certificate authority generates the certificate and then signs it with it's own. This signiture prevents the certificate from being tampered with since the certificate would no longer match the signed details.

We can create our own certificate authority using OpenSSL and sign our own certificates. In the real world nothing uses self-signed certificates since they can not be verified to determine if they have been tampered with. Usually you would send your certificate signing request to a large company and pay to have it signed. A  certificate authority certificate however is more complex than a normal certificate and requires a configuration file to tell OpenSSL how to interact with it. You can find a template configuration file [here](./openssl.conf).

To create the authority you must first setup an environment for it;

```bash
mkdir certs
mkdir crl
mkdir csr
mkdir newcerts
mkdir private

touch index.txt
echo 1000 > crlnumber
echo 1000 > serial
```

Within this folder you must add an `openssl.conf` file which configures OpenSSL to use the authority. We can now create a key-pair that the authority will use for signing;

> `openssl genrsa -aes256 -out private/signing.key 4096`

You must remember to `chmod 400 private/signing.key` the key before using it. We can now generate the signing certificate for the authority;

> `openssl req -new -x509 -days 7300 -sha256 -config openssl.conf -key private/signing.key -extensions v3_ca -out certs/$signing.crt`

This command will generate a self signed authority certificate which is able to sign other certificates. This self signed certificate is known as the root certificate and is not often used by proper certificate authorities. Instead another intermediate authority is created and signed by the root authority and that is used instead. This means the root authority can be kept off of the internet and safe while the intermediate is used. If the intermediate authority is compromised then it can easily be revoked and changed without compromising the entire chain of trust.

To create an intermediate authority create another folder with the same layout and structure and create a configuration file and signing key-pair.  But this time we will create a certificate signing request and sign it with our root authority;

```bash
openssl req -config openssl.conf -new -sha256 \
  -key private/signing.key \
  -out csr/signing.csr

openssl ca -config ../root/openssl.conf -extensions v3_intermediate_ca \
  -days 3650 -notext -md sha256 \
  -in csr/signing.csr \
  -out certs/signing.crt
```
